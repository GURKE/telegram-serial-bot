/**
 * @file main.cpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2021-07-19
 *
 * Remarks:
 *   Maximal single Telegram message length: 1455 Bytes / Char
 */
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>   // Universal Telegram Bot Library written by Brian Lough: https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot
#include "PSK.h"

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

const int botRequestDelay = 100;
unsigned long lastTimeBotRan;

const int ledPin = 2;
bool ledState = LOW;

bool silent = false;

// Handle what happens when you receive new messages
void handleNewMessages(int numNewMessages) {
  for (int i = 0; i < numNewMessages; i++) {
    // Chat id of the requester
    String chat_id = String(bot.messages[i].chat_id);
    if (chat_id == CHAT_ID) {
      String text = bot.messages[i].text;
      if (text == "/silent") {
        silent = true;
        bot.sendMessage(CHAT_ID, "Changed to silent mode!", "");
      } else if (text == "/loud") {
        silent = false;
        bot.sendMessage(CHAT_ID, "Changed to loud mode!", "");
      } else
        Serial.print(text);
    }
  }
}

void setup() {
  Serial.setRxBufferSize(10000);
  Serial.begin(57600);
  client.setInsecure();
  WiFi.mode(WIFI_STA);
  uint8_t i = 0;

  while (1) {
    WiFi.begin(ssid[i], password[i]);
    uint8_t tries = 0;
    while (WiFi.status() != WL_CONNECTED && tries < 10) {
      delay(1000);
      tries++;
    }

    if (WiFi.status() == WL_CONNECTED)
      break;

    i++;
    if (i == max_wifi)
      i = 0;
  }

  char str[100];
  IPAddress ip = WiFi.localIP();
  sprintf(str, "CNC-Konsole online, IP-Address: %u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
  bot.sendMessage(CHAT_ID, str, "");
  bot.sendMessage(CHAT_ID, "/silent - activates silent mode\n/loud - deactivates silent mode", "");

}

void loop() {
  if (millis() > lastTimeBotRan + botRequestDelay) {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while (numNewMessages) {
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }
    lastTimeBotRan = millis();
  }

  if (Serial.available()) {
    String s = Serial.readString();
    static bool silentmode = false;
    if (!silentmode && s[0] == '!' && s[1] == '<') {
      silentmode = true;
      bot.sendMessage(CHAT_ID, "Changed to CNC silent mode!", "");
    } else if (silentmode && s[0] == '!' && s[1] == '>') {
      silentmode = false;
      bot.sendMessage(CHAT_ID, "Changed to CNC loud mode!", "");
    } else {
      if (!silentmode || !silent || (silentmode && s[0] == '!'))
        bot.sendMessage(CHAT_ID, s, "");
    }
  }
}
