/**
 * @file PSK.h
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief Contains Wifi-SSIDs, PSKs, Bottoken and Chat_ID
 * @version 0.1
 * @date 2021-07-19
 *
 * Remarks:
*/
#pragma once

#include <stdint.h>

// List of WiFi SSIDs, ESP will try them from left to right
const char* ssid[] = { "SSID1", "SSID2", "SSID3" };

// List of WiFi PSK to the defined SSIDs
const char* password[] = { "PSK of SSID 1", "PSK of SSID 2", "PSK of SSID 3" };

// Number of defined WiFis
const uint8_t max_wifi = 3;

// Telegram Bot Token (Get from Botfather)
#define BOTtoken "##########:***********************************"

// Use @myidbot to find out the chat ID of an individual or a group. Also note that you need to click "start" on a bot before it can message you
#define CHAT_ID "-#########"